package repository;
import models.Book;
import play.db.jpa.JPAApi;
import javax.inject.*;
import javax.persistence.*;
import java.util.List;
import java.util.concurrent.*;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.util.concurrent.CompletableFuture.supplyAsync;

@Singleton
public class BookRepository {
  private final JPAApi jpaApi ;


  @Inject
  public BookRepository(JPAApi jpaApi) {
    this.jpaApi = jpaApi;
  }

  public CompletionStage<Book> add(Book book) {
    return supplyAsync(() -> wrap(em -> insert(em, book)));
  }

  public CompletionStage<Stream<Book>> list() {
    return supplyAsync(() -> wrap(em -> list(em)));
  }

  public CompletionStage<Book> get(Long id){
    return supplyAsync(() -> wrap(em -> getBook(em, id)));
  }

  public CompletionStage<Book> update(Book book){
    return supplyAsync(() -> wrap(em -> updateBook(em, book)));
  }

  public CompletionStage<Boolean> delete(Long id){
    return supplyAsync(() -> wrap(em -> remove(em, id)));
  }

  private <T> T wrap(Function<EntityManager, T> function) {
    return jpaApi.withTransaction(function);
  }

  private Book insert(EntityManager em, Book book) {
    em.persist(book);
    return book;
  }

  private Stream<Book> list(EntityManager em) {
    List<Book> books = em.createQuery("select b from book b", Book.class).getResultList();
    return books.stream();
  }

  private Book updateBook(EntityManager em, Book newBook){
    Book oldBook = em.find(Book.class, newBook.getId());
    oldBook.update(newBook);
    return oldBook;
  }

  private Book getBook(EntityManager em, Long id) {
    return em.find(Book.class, id);
  }

  private Boolean remove(EntityManager em, Long id){
    Book book = em.find(Book.class, id);
    if (book == null){
      return false;
    }else {
      em.remove(book);
      return true;
    }
  }
}