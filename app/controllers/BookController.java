package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.Book;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import services.DefaultBookService;

import javax.inject.Inject;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;


public class BookController extends Controller {

  private DefaultBookService BS;

  @Inject
  public BookController(DefaultBookService BS){this.BS = BS;}

  public CompletionStage<Result> getAll(String q) {
    return BS.get().thenApplyAsync(bookStream -> ok(Json.toJson(bookStream.collect(Collectors.toList()))));
  }

  public CompletionStage<Result> newBook(Http.Request request) {
    JsonNode jsonbook = request.body().asJson();
    Book newbook = Json.fromJson(jsonbook, Book.class);
    return BS.add(newbook).thenApplyAsync(book -> ok(Json.toJson(book)));
  }

  public CompletionStage<Result> changeBook(Http.Request request, Long id) {
    JsonNode jsonbook = request.body().asJson();
    Book updatebook = Json.fromJson(jsonbook, Book.class);
    return BS.update(updatebook).thenApplyAsync(book -> ok(Json.toJson(book)));
  }

  public CompletionStage<Result> getBook(long id) {
    return BS.get(id).thenApplyAsync(book -> ok(Json.toJson(book)));
  }

  public CompletionStage<Result> deleteBook(long id) {
    return BS.delete(id).thenApplyAsync(aBoolean -> ok());
  }
}
