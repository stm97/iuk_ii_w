package services;

import models.Book;
import repository.BookRepository;

import javax.inject.Inject;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

public class DefaultBookService implements BookService {


  private BookRepository bookRepository;

  @Inject
  public DefaultBookService(BookRepository bookRepository) {
    this.bookRepository = bookRepository;
  }

  public CompletionStage<Stream<Book>> get() {
    return bookRepository.list();
  }

  public CompletionStage<Book> get(final Long id) {
    return  bookRepository.get(id);
  }

  public CompletionStage<Boolean> delete(Long id) {
    return bookRepository.delete(id);
  }

  public CompletionStage<Book> update(Book book) {
      return bookRepository.update(book);
  }

  public CompletionStage<Book> add(Book book) {
    return bookRepository.add(book);
  }

  public Book getDummy(Long i){
    final Book book = new Book();
    book.setId(i);
    book.setDescription("Beschreibung von Buch: " + i);
    book.setIsbn10(i + "4851384568");
    book.setIsbn13(i + "854687241568");
    book.setPages(50);
    book.setPublisher("Publisher" + i);
    book.setTitle("Titel Buch" + 1);
    return book;
  }
}

