package services;

import com.fasterxml.jackson.databind.JsonNode;

import javax.inject.Inject;

import java.util.ArrayList;
import java.util.List;
import models.Book;
import models.nytimes.NYTimesBestseller;
import models.nytimes.NYTimesBook;
import models.nytimes.NYTimesReviewList;
import play.libs.Json;
import play.libs.ws.*;
import java.util.concurrent.CompletionStage;

public class DefaultNYTimesService implements NYTimesService {

  private final WSClient ws;

  @Inject
  public DefaultNYTimesService(WSClient ws){
    this.ws = ws;
  }

  @Override
  public CompletionStage<List<Book>> bestseller() {
    final String url = "https://api.nytimes.com/svc/books/v3/lists/overview.json";
    final WSRequest request = ws.url(url).addQueryParameter("api-key", "6DofT2l5Gvt7Fk1prdGMOYyQfySgbP4v");
    final CompletionStage<JsonNode> jsonPromise = request.get().thenApply(result -> result.asJson());
    final CompletionStage<NYTimesBestseller> bestsellerPromise = jsonPromise.thenApplyAsync(json -> Json.fromJson(json, NYTimesBestseller.class));
    final CompletionStage<List<Book>> booksPromise = bestsellerPromise.thenApplyAsync(this::map);
    return booksPromise;
  }


  private List<Book> map(NYTimesBestseller bestseller) {
    final List<Book> books = new ArrayList<>();
    for(NYTimesReviewList list : bestseller.getResults().getLists()){
      if (list.getList_id()==704){
        list.getBooks().forEach(nyTimesBook -> books.add(map(nyTimesBook)));
      }
    }
    return books;
  }

  private Book map(NYTimesBook nyTimesBook){
    final Book book =  new Book();
    book.setDescription(nyTimesBook.getDescription());
    book.setIsbn10(nyTimesBook.getPrimary_isbn10());
    book.setIsbn13(nyTimesBook.getPrimary_isbn13());
    book.setTitle(nyTimesBook.getTitle());
    book.setPublisher(nyTimesBook.getPublisher());
    return book;
  }
}
