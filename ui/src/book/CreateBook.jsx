import React from 'react';


class CreateBook extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: "Mein Buch",
            publisher: "new Publisher"
          };
          this.handleInputChange = this.handleInputChange.bind(this);
          this.handleSubmit = this.handleSubmit.bind(this);
  
    }

    handleInputChange(event){
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState(
            {[name]: value}
        );
    }

    handleSubmit(){
    const book= 
           {
            'title': this.state.title,
            'isbn13': null,
            'isbn10': null,
            'description': null,
            'publisher': this.state.publisher,
            'pages': null
           }
        const jbook = JSON.stringify(book)
        fetch('/api/books', {
            method: 'POST', body: jbook, headers: {
                'Content-Type': 'application/json',
                
            }
        });
    }

    render() { 
        return ( 
            <form onSubmit={this.handleSubmit}>
                <label>
                    Titel:
                    <input type="text" name="title" value={this.state.title}
                        onChange={this.handleInputChange}
                    />
               
                    Publisher:
                    <input type="text" name="publisher" value={this.state.publisher}
                        onChange={this.handleInputChange}
                    />
                </label>
                <input type="submit" value="Create Book"/>
            </form>
         );
    }
}
 
export default CreateBook;