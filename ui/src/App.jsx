import React from 'react';
import logo from './book.svg';
import './App.css';
import Books from './book/Books.jsx';
import CreateBook from './book/CreateBook.jsx'

import{
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from 'react-router-dom'

function App() {
  return (
    <div className="App">
      <Router>
      <header className="App">
        <img src={logo} className="App-logo" alt="logo"/>
      
    <p>
      Welcome to our Book store.
    </p>
    <nav>
      <Link to="/">Home</Link> |
      <Link to="/books">Books</Link> | 
      <Link to="/create">CreateBook</Link>
    </nav>
    </header>
    <Switch>
      <Route path="/books">
        <Books />
      </Route>
      <Route path="/create">
        <CreateBook />
      </Route>
      <Route path="/">
        <p>home Komponente</p>
      </Route>
    </Switch>
    </Router>
    </div>
  );
}

export default App;
