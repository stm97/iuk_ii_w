-- Book schema
-- !Ups

create table book(
                     id integer (20) NOT NULL Auto_increment,
                     title varchar(50),
                     isbn13 varchar(13),
                     isbn10 varchar(10),
                     description varchar(100),
                     publisher varchar(50),
                     pages integer ,
                     Primary key (id)
);
-- !Downs
DROP TABLE book;